import App from "./app.js";
import ImageController from "./image/image.controller.js";
import scheduleCron from "./utils/cron.js";

const app = new App([new ImageController()]);

app.listen();

scheduleCron();

import * as express from "express";
import Controller from "../interfaces/controller.js";
import HttpException from "../exceptions/HttpException.js";
import UploadNoPayloadException from "../exceptions/UploadNoPayloadException.js";
import imageDb from "./image.model.js";
import uploadMiddleware from "../middlewares/upload.js";
import processImageFile from "../utils/processImageFile.js";
import processImageUrl from "../utils/processImageUrl.js";
import { isUploadLimit } from "../utils/fs.js";
import { UPLOAD_DESTINATION_PATH } from "../utils/config.js";

class ImageController implements Controller {
  public path = "/image";
  public router = express.Router();
  private db = imageDb;
  private uploadPath = UPLOAD_DESTINATION_PATH;

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(this.path, this.getImages);
    this.router.post(this.path, uploadMiddleware("file"), this.postImage);
    this.router.delete(`${this.path}/:id`, this.deleteImage);
  }

  private getImages = async (
    request: express.Request,
    response: express.Response
  ) => {
    const userId = request.session?.id;
    if (!userId) {
      throw new Error("User id was not set on session.");
    }

    const { lastId, count, labelFilter, before } = request.query;
    const parsedQuery = {
      lastId: typeof lastId === "string" ? lastId : "",
      count:
        typeof count === "string" && count.match(/^[123456789]\d*$/)
          ? Number(count)
          : 10,
      labelFilter: typeof labelFilter === "string" ? labelFilter : "",
      before: before === "false" ? false : true,
    };

    const imagesData = await this.db.getImages(
      userId,
      parsedQuery.lastId,
      parsedQuery.count,
      parsedQuery.labelFilter,
      parsedQuery.before
    );
    const isFetchedAll = imagesData.length < parsedQuery.count;
    return response.json({
      images: imagesData,
      isFetchedAll,
    });
  };

  private postImage = async (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction
  ) => {
    const userId = request.session?.id;
    if (!userId) {
      throw new Error("User id was not set on session.");
    }

    if (await isUploadLimit()) {
      return next(
        new HttpException(
          507,
          "Storage limit is reached. Please try again in a few days."
        )
      );
    }

    const { label, url }: { label?: string; url?: string } = request.body;
    const { file } = request;
    const { timestampUnix: timestampMs } = request;

    if (!url && !file) {
      return next(new UploadNoPayloadException());
    }

    let processedImage: { url: string; width: number; height: number };

    try {
      processedImage = url
        ? await processImageUrl(url)
        : await processImageFile(
            (file as Express.Multer.File).path,
            `${this.uploadPath}/${userId}`
          );
    } catch (e) {
      if (e instanceof Error && e.message === "invalid url") {
        return next(new HttpException(400, "Invalid url: image not found"));
      }
      throw e;
    }

    const imageData = {
      ...processedImage,
      label: label || "",
      id: String(timestampMs as number),
    };
    await this.db.putImage(userId, imageData);

    return response.json(imageData);
  };

  private deleteImage = (
    request: express.Request,
    response: express.Response
  ) => {
    const userId = request.session?.id;
    if (!userId) {
      throw new Error("User id was not set on session.");
    }

    const imageId = request.params.id;
    this.db.deleteImage(userId, imageId).then(() => {
      return response.sendStatus(205);
    });
  };
}

export default ImageController;

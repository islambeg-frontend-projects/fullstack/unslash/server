interface Image {
  id: string;
  label: string;
  url: string;
  width: number;
  height: number;
}

export default Image;

import * as fs from "fs";
import { Level } from "level";
import { AbstractSublevel } from "abstract-level";
import type Image from "./image.interface.js";
import { getCurrentTimestampMs } from "../utils/datetime.js";
import {
  LEVELDB_PATH,
  DEFAULT_IMAGES_CONFIG_PATH,
  STORAGE_MAX_DURATION_MS,
} from "../utils/config.js";

const defaultImages: Image[] = JSON.parse(
  fs.readFileSync(DEFAULT_IMAGES_CONFIG_PATH).toString()
);

const defaultInsertions: {
  type: "put";
  key: string;
  value: Image;
}[] = defaultImages.map((img, idx) => {
  const imageId = String(getCurrentTimestampMs() + idx);
  return {
    type: "put",
    key: String(getCurrentTimestampMs() + idx),
    value: { ...img, id: imageId },
  };
});

type ImageTable = Level<string, Image>;

type ImageSubtable = AbstractSublevel<
  ImageTable,
  string | Buffer | Uint8Array,
  string,
  Image
>;

class ImageDB {
  private location: string;
  private conn?: ImageTable;

  constructor(location: string) {
    this.location = location;
  }

  getImages(
    userId: string,
    lastId: string,
    count: number,
    labelFilter: string,
    before = true
  ) {
    return labelFilter
      ? this.getImagesWithFilter(userId, lastId, count, labelFilter, before)
      : this.getImagesWithoutFilter(userId, lastId, count, before);
  }

  getImagesWithoutFilter(
    userId: string,
    lastId: string,
    count: number,
    before = true
  ) {
    return this.getUserTable(userId).then(async (table) =>
      lastId
        ? before
          ? table.values({ lt: lastId, limit: count, reverse: true }).all()
          : table.values({ gt: lastId, limit: count }).all()
        : table.values({ limit: count, reverse: before }).all()
    );
  }

  async getImagesWithFilter(
    userId: string,
    lastId: string,
    count: number,
    labelFilter: string,
    before = true
  ) {
    const table = await this.getUserTable(userId);
    const iterator = lastId
      ? before
        ? table.values({ lt: lastId, reverse: true })
        : table.values({ gt: lastId })
      : table.values({ reverse: before });
    const fetchedImages: Image[] = [];

    do {
      const nextImage = await iterator.next();

      if (!nextImage) {
        break;
      }

      if (
        !nextImage.label
          .toLocaleLowerCase()
          .includes(labelFilter.toLocaleLowerCase())
      ) {
        continue;
      }

      fetchedImages.push(nextImage);
    } while (fetchedImages.length < count);

    return fetchedImages;
  }

  putImage(userId: string, image: Image) {
    return this.getUserTable(userId).then((table) =>
      table.put(image.id, image)
    );
  }

  deleteImage(userId: string, imageId: string) {
    return this.getUserTable(userId).then((table) => table.del(imageId));
  }

  // arrow function is used so that it'll be easier to pass it as a callback
  clearOld = async () => {
    const conn = await this.getConnection();
    const allKeys = await conn.keys().all();
    const keysForDeletion = allKeys.filter((k) => {
      const timestampMatch = k.match(/!.+?!(\d+)/);
      if (!timestampMatch) {
        return true;
      }
      const keyTimestampMs = Number(timestampMatch[1]);
      const msSinceInsertion = getCurrentTimestampMs() - keyTimestampMs;
      return msSinceInsertion > STORAGE_MAX_DURATION_MS;
    });

    const batchDeletions: { type: "del"; key: string }[] = keysForDeletion.map(
      (key) => ({
        type: "del",
        key,
      })
    );
    return conn.batch(batchDeletions);
  };

  private async getUserTable(userId: string) {
    const conn = this.getConnection();
    const userTable: ImageSubtable = conn.sublevel(userId, {
      valueEncoding: "json",
    });
    const isNew = await this.isNewUser(userId);
    if (isNew) {
      await this.populateWithDefaults(userTable);
    }
    return userTable;
  }

  private getConnection() {
    if (!this.conn) {
      this.conn = new Level(this.location);
    }

    return this.conn;
  }

  private populateWithDefaults(userTable: ImageSubtable) {
    return userTable.batch(defaultInsertions);
  }

  private isNewUser(userId: string) {
    return this.getConnection()
      .sublevel(userId)
      .keys()
      .next()
      .then((k) => k == undefined);
  }
}

export default new ImageDB(LEVELDB_PATH);

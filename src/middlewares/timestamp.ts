import { NextFunction, Request, Response } from "express";

declare module "express-serve-static-core" {
  interface Request {
    timestampUnix?: number;
  }
}

function timestampMiddleware(
  request: Request,
  response: Response,
  next: NextFunction
) {
  request.timestampUnix = Date.now();
  next();
}

export default timestampMiddleware;

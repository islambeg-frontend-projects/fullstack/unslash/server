import { NextFunction, Request, Response } from "express";
import cookieSession from "cookie-session";
import { nanoid } from "nanoid/async";
import { ENV, STORAGE_MAX_DURATION_MS } from "../utils/config.js";

function sessionMiddleware(
  cookieOptions?: Omit<CookieSessionInterfaces.CookieSessionOptions, "name">
) {
  return cookieSession({
    name: "session",
    secret: process.env.COOKIE_SECRET || "secret",
    maxAge: STORAGE_MAX_DURATION_MS,
    sameSite: true,
    secure: ENV === "production",
    ...cookieOptions,
  });
}

async function sessionIdMiddleware(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (!req.session) {
    return;
  }

  if (!req.session.id) {
    req.session.id = await nanoid();
  }

  return next();
}

export { sessionMiddleware, sessionIdMiddleware };

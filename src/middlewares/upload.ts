import * as express from "express";
import multer from "multer";
import mime from "mime-types";
import FileTooLargeException from "../exceptions/FileTooLargeException.js";
import FileWrongTypeException from "../exceptions/FileWrongTypeException.js";
import { UPLOAD_TMP_PATH, UPLOAD_MAX_SIZE_BYTES } from "../utils/config.js";

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, `${UPLOAD_TMP_PATH}`);
  },

  filename: function (req, file, cb) {
    if (!req.timestampUnix) {
      throw new Error("Timestamp is not set on request.");
    }

    const timestampMs = req.timestampUnix;
    const filename = `${timestampMs}.${mime.extension(file.mimetype)}`;
    cb(null, filename);
  },
});

const upload = multer({
  storage,
  limits: {
    fileSize: UPLOAD_MAX_SIZE_BYTES,
  },
  fileFilter: (req, file, cb) => {
    if (!isImage(file)) {
      return cb(new FileWrongTypeException());
    }

    return cb(null, true);
  },
});

function uploadMiddleware(fileField: string) {
  return function (
    request: express.Request,
    response: express.Response,
    next: express.NextFunction
  ) {
    upload.single(fileField)(request, response, (error) => {
      if (isFileSizeError(error)) {
        return next(new FileTooLargeException(UPLOAD_MAX_SIZE_BYTES));
      } else if (error) {
        return next(error);
      }

      return next();
    });
  };
}

export default uploadMiddleware;

function isImage(file: Express.Multer.File) {
  return file.mimetype.startsWith("image/");
}

function isFileSizeError(error: unknown) {
  return (
    error instanceof multer.MulterError && error.code === "LIMIT_FILE_SIZE"
  );
}

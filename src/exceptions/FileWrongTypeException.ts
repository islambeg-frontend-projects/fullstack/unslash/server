import HttpException from "./HttpException.js";

class FileWrongType extends HttpException {
  constructor() {
    super(400, "Only files of type image/* are allowed.");
  }
}

export default FileWrongType;

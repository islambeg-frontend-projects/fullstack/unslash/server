import HttpException from "./HttpException.js";

class InvalidImageUrlException extends HttpException {
  constructor() {
    super(400, "Valid image was not found at the provided URL.");
  }
}

export default InvalidImageUrlException;

import HttpException from "./HttpException.js";

class UploadNoPayloadException extends HttpException {
  constructor() {
    super(400, "Either URL or file must be provided.");
  }
}

export default UploadNoPayloadException;

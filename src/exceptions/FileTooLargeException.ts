import HttpException from "./HttpException.js";

class FileTooLarge extends HttpException {
  constructor(fileLimitBytes: number) {
    const fileLimitMB = Math.floor(fileLimitBytes / (1024 * 1024));
    super(415, `File too large. Maximum allowed size is ${fileLimitMB}MB.`);
  }
}

export default FileTooLarge;

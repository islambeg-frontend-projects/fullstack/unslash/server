import * as fs from "fs";
import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import Controller from "./interfaces/controller.js";
import timestampMiddleware from "./middlewares/timestamp.js";
import {
  sessionMiddleware,
  sessionIdMiddleware,
} from "./middlewares/session.js";
import errorMiddleware from "./middlewares/error.js";
import { DOMAIN_NAME, ENV, PORT, UPLOAD_TMP_PATH } from "./utils/config.js";

class App {
  public app: express.Application;

  constructor(controllers: Controller[]) {
    this.app = express();
    this.app.set("trust proxy", 1);

    this.initializeDatabase();
    this.initializeMiddlewares();
    this.initializeControllers(controllers);
    this.initializeErrorHandling();
  }

  public listen() {
    this.app.listen(PORT, () => {
      console.log(`App listening on the port ${PORT}`);
    });
  }

  private initializeDatabase() {
    const tmpStoragePath = `${UPLOAD_TMP_PATH}`;
    if (!fs.existsSync(tmpStoragePath)) {
      fs.mkdirSync(tmpStoragePath);
    }
  }

  private initializeMiddlewares() {
    if (ENV === "production") {
      this.app.use(cors({ origin: `*.${DOMAIN_NAME}` }));
    }
    this.app.use(timestampMiddleware);
    this.app.use(sessionMiddleware(), sessionIdMiddleware);
    this.app.use(bodyParser.json());
  }

  private initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private initializeControllers(controllers: Controller[]) {
    controllers.forEach((controller) => {
      this.app.use("/api", controller.router);
    });
  }
}

export default App;

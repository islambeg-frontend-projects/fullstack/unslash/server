function getCurrentTimestampMs() {
  return Date.now();
}

export { getCurrentTimestampMs };

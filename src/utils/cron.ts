import cron from "node-cron";
import { clearOldFiles } from "./fs.js";
import imageDb from "../image/image.model.js";
import { UPLOAD_TMP_PATH, UPLOAD_DESTINATION_PATH } from "./config.js";

const CRON_EVERY_FIVE_MINUTES = "*/5 * * * *";

export default function schedule() {
  cron.schedule(CRON_EVERY_FIVE_MINUTES, async () => {
    await clearOldFiles(UPLOAD_TMP_PATH);
    await clearOldFiles(UPLOAD_DESTINATION_PATH);
  });

  cron.schedule(CRON_EVERY_FIVE_MINUTES, imageDb.clearOld);
}

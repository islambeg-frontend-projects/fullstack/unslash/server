import probeImageSize from "probe-image-size";
import InvalidImageUrlException from "../exceptions/InvalidImageUrlException.js";

async function processImageUrl(url: string) {
  try {
    const { width, height } = await probeImageSize(url);
    return { url, width, height };
  } catch (e) {
    const isInvalidUrl =
      e instanceof Error && e.message.includes("getaddrinfo ENOTFOUND");
    const isInvalidImageFormat =
      e instanceof probeImageSize.Error &&
      e.message === "unrecognized file format";

    if (isInvalidUrl || isInvalidImageFormat) {
      throw new InvalidImageUrlException();
    } else {
      throw e;
    }
  }
}

export default processImageUrl;

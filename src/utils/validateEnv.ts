import { cleanEnv, port, str, num } from "envalid";

export default function validateEnv() {
  cleanEnv(process.env, {
    DOMAIN_NAME: str(),
    NODE_ENV: str(),
    PORT: port(),
    LEVELDB_PATH: str(),
    DEFAULT_IMAGES_CONFIG_PATH: str(),
    UPLOAD_TMP_PATH: str(),
    UPLOAD_DESTINATION_PATH: str(),
    UPLOAD_MAX_SIZE_BYTES: num(),
    STORAGE_MAX_DURATION_MS: num(),
    STORAGE_MAX_SIZE_BYTES: num(),
  });
}

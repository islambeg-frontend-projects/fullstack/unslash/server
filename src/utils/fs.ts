import fs from "fs/promises";
import path from "path";
import { getCurrentTimestampMs } from "./datetime.js";
import {
  UPLOAD_TMP_PATH,
  STORAGE_MAX_DURATION_MS,
  STORAGE_MAX_SIZE_BYTES,
} from "./config.js";

async function getTmpUploadsSize() {
  const tmpPath = UPLOAD_TMP_PATH as string;
  const imageNames = await fs.readdir(tmpPath);
  let totalSize = 0;
  for (const imgName of imageNames) {
    const imgPath = path.join(tmpPath, imgName);
    const imgSize = await fs.stat(imgPath).then((stat) => stat.size);
    totalSize += imgSize;
  }

  return totalSize;
}

async function isUploadLimit() {
  return getTmpUploadsSize().then(
    (currentSize) => currentSize > STORAGE_MAX_SIZE_BYTES
  );
}

async function clearOldFiles(dest: string) {
  const isDirectory = await fs.stat(dest).then((stat) => stat.isDirectory());
  if (!isDirectory) {
    return;
  }

  const destChildrenNames = await fs.readdir(dest);
  for (const childName of destChildrenNames) {
    // do not touch default images
    if (childName.match(/default/)) {
      continue;
    }

    const childPath = path.join(dest, childName);
    const childCreatedAt = await fs
      .stat(childPath)
      .then((stat) => stat.birthtimeMs);
    const msSinceCreation = getCurrentTimestampMs() - childCreatedAt;
    if (msSinceCreation > STORAGE_MAX_DURATION_MS) {
      await fs.rm(childPath, { recursive: true, force: true });
    }
  }
}

export { isUploadLimit, clearOldFiles };

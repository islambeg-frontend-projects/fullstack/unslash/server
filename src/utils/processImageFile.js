// couldn't make imagemin import work with typescript
// thus it was converted to plain js

import * as fs from "fs";
import imagemin from "imagemin";
import imageminJpeg from "imagemin-jpegtran";
import imageminPng from "imagemin-optipng";
import imageminGif from "imagemin-gifsicle";
import imageminSvg from "imagemin-svgo";
import probeImageSize from "probe-image-size";

/**
 * @param {string[]} paths
 * @param {string} dest
 */
async function minify(paths, dest) {
  return imagemin(paths, {
    destination: dest,
    plugins: [
      imageminJpeg(),
      imageminPng(),
      imageminGif(),
      imageminSvg({
        plugins: [
          {
            name: "removeViewBox",
            active: false,
          },
        ],
      }),
    ],
  });
}

/**
 * @param {string} filepath
 * @param {string} outputDest
 * @returns {{url: string; width: number; height: number}}
 */
async function processImageFile(filepath, outputDest) {
  const [{ destinationPath }] = await minify([filepath], outputDest);

  const { width, height } = await probeImageSize(
    fs.createReadStream(destinationPath)
  );

  const pathForUrl = destinationPath.split("/").slice(-3).join("/");

  return { url: `/${pathForUrl}`, width, height };
}

export default processImageFile;

import "dotenv/config";
import validateEnv from "./validateEnv.js";

validateEnv();

const DOMAIN_NAME = process.env.DOMAIN_NAME;
const ENV = process.env.NODE_ENV;
const PORT = Number(process.env.PORT);
const LEVELDB_PATH = process.env.LEVELDB_PATH as string;
const DEFAULT_IMAGES_CONFIG_PATH = process.env.DEFAULT_IMAGES_CONFIG_PATH as string;
const UPLOAD_TMP_PATH = process.env.UPLOAD_TMP_PATH as string;
const UPLOAD_DESTINATION_PATH = process.env.UPLOAD_DESTINATION_PATH as string;
const UPLOAD_MAX_SIZE_BYTES = Number(process.env.UPLOAD_MAX_SIZE_BYTES);
const STORAGE_MAX_DURATION_MS = Number(process.env.STORAGE_MAX_DURATION_MS);
const STORAGE_MAX_SIZE_BYTES = Number(process.env.STORAGE_MAX_SIZE_BYTES);

export {
  DOMAIN_NAME,
  ENV,
  PORT,
  LEVELDB_PATH,
  DEFAULT_IMAGES_CONFIG_PATH,
  UPLOAD_TMP_PATH,
  UPLOAD_DESTINATION_PATH,
  UPLOAD_MAX_SIZE_BYTES,
  STORAGE_MAX_DURATION_MS,
  STORAGE_MAX_SIZE_BYTES,
};
